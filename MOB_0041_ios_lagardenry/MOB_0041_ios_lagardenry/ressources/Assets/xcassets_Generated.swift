// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  internal typealias AssetColorTypeAlias = NSColor
  internal typealias Image = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  internal typealias AssetColorTypeAlias = UIColor
  internal typealias Image = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

@available(*, deprecated, renamed: "ImageAsset")
internal typealias MOBXcassetsType = ImageAsset

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  internal var image: Image {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

internal struct ColorAsset {
  internal fileprivate(set) var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  internal var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum MOBXcassets {
  internal static let _001Sun = ImageAsset(name: "001-sun")
  internal static let _033Drops = ImageAsset(name: "033-drops")
  internal static let guillaumeTPola = ImageAsset(name: "GuillaumeT_pola")
  internal static let lineSplashscreen = ImageAsset(name: "Line-splashscreen")
  internal static let addSeed1 = ImageAsset(name: "add-seed-1")
  internal static let addSeed = ImageAsset(name: "add-seed")
  internal static let arrow = ImageAsset(name: "arrow")
  internal static let bgPattern = ImageAsset(name: "bg-pattern")
  internal static let checkboxFilled = ImageAsset(name: "checkbox-filled")
  internal enum Color {
    internal static let accentAction = ColorAsset(name: "AccentAction")
    internal static let bleuFond = ColorAsset(name: "BleuFond")
    internal static let bleuTitre = ColorAsset(name: "BleuTitre")
    internal static let clearWhite10 = ColorAsset(name: "ClearWhite10")
    internal static let clearWhite30 = ColorAsset(name: "ClearWhite30")
  }
  internal static let home = ImageAsset(name: "home")
  internal static let iconStarEmpty = ImageAsset(name: "icon-star-empty")
  internal static let iconStarFull = ImageAsset(name: "icon-star-full")
  internal static let iconUserProfile = ImageAsset(name: "icon-user-profile")
  internal static let iconsSplashscreen = ImageAsset(name: "icons-splashscreen")
  internal static let imageMenuCultiver = ImageAsset(name: "image-menu-cultiver")
  internal static let imageMenuPlanter = ImageAsset(name: "image-menu-planter")
  internal static let imageMenuPotager = ImageAsset(name: "image-menu-potager")
  internal enum Legume {
    internal static let acorn = ImageAsset(name: "acorn")
    internal static let ail = ImageAsset(name: "ail")
    internal static let apple = ImageAsset(name: "apple")
    internal static let apricot = ImageAsset(name: "apricot")
    internal static let artichoke = ImageAsset(name: "artichoke")
    internal static let asparagus = ImageAsset(name: "asparagus")
    internal static let avocado = ImageAsset(name: "avocado")
    internal static let bamboo = ImageAsset(name: "bamboo")
    internal static let bananas = ImageAsset(name: "bananas")
    internal static let bellPepper = ImageAsset(name: "bell_pepper")
    internal static let betterave = ImageAsset(name: "betterave")
    internal static let blackberry = ImageAsset(name: "blackberry")
    internal static let blueberry = ImageAsset(name: "blueberry")
    internal static let breastMilkFruit = ImageAsset(name: "breast_milk_fruit")
    internal static let broccoli = ImageAsset(name: "broccoli")
    internal static let brussels = ImageAsset(name: "brussels")
    internal static let cabbage = ImageAsset(name: "cabbage")
    internal static let carrot = ImageAsset(name: "carrot")
    internal static let cauliflower = ImageAsset(name: "cauliflower")
    internal static let celery = ImageAsset(name: "celery")
    internal static let cherry = ImageAsset(name: "cherry")
    internal static let chili = ImageAsset(name: "chili")
    internal static let coconut = ImageAsset(name: "coconut")
    internal static let corn = ImageAsset(name: "corn")
    internal static let courge = ImageAsset(name: "courge")
    internal static let courgette = ImageAsset(name: "courgette")
    internal static let cucumber = ImageAsset(name: "cucumber")
    internal static let currant = ImageAsset(name: "currant")
    internal static let durian = ImageAsset(name: "durian")
    internal static let eggplant = ImageAsset(name: "eggplant")
    internal static let figue = ImageAsset(name: "figue")
    internal static let ginger = ImageAsset(name: "ginger")
    internal static let grape = ImageAsset(name: "grape")
    internal static let grapefruit = ImageAsset(name: "grapefruit")
    internal static let kiwi = ImageAsset(name: "kiwi")
    internal static let lemon = ImageAsset(name: "lemon")
    internal static let lettuce = ImageAsset(name: "lettuce")
    internal static let lightGreen = ImageAsset(name: "light_green")
    internal static let lightOrange = ImageAsset(name: "light_orange")
    internal static let lightRed = ImageAsset(name: "light_red")
    internal static let mandarin = ImageAsset(name: "mandarin")
    internal static let mango = ImageAsset(name: "mango")
    internal static let mangosteen = ImageAsset(name: "mangosteen")
    internal static let melon = ImageAsset(name: "melon")
    internal static let mirabelle = ImageAsset(name: "mirabelle")
    internal static let mushroom = ImageAsset(name: "mushroom")
    internal static let olive = ImageAsset(name: "olive")
    internal static let onion = ImageAsset(name: "onion")
    internal static let orange = ImageAsset(name: "orange")
    internal static let papaya = ImageAsset(name: "papaya")
    internal static let parsley = ImageAsset(name: "parsley")
    internal static let peach = ImageAsset(name: "peach")
    internal static let peanut = ImageAsset(name: "peanut")
    internal static let pear = ImageAsset(name: "pear")
    internal static let peas = ImageAsset(name: "peas")
    internal static let pineapple = ImageAsset(name: "pineapple")
    internal static let pitaya = ImageAsset(name: "pitaya")
    internal static let poireau = ImageAsset(name: "poireau")
    internal static let pomegranate = ImageAsset(name: "pomegranate")
    internal static let potato = ImageAsset(name: "potato")
    internal static let prune = ImageAsset(name: "prune")
    internal static let pumpkin = ImageAsset(name: "pumpkin")
    internal static let quince = ImageAsset(name: "quince")
    internal static let radish = ImageAsset(name: "radish")
    internal static let rambutan = ImageAsset(name: "rambutan")
    internal static let raspberry = ImageAsset(name: "raspberry")
    internal static let rhubarb = ImageAsset(name: "rhubarb")
    internal static let roseApple = ImageAsset(name: "rose_apple")
    internal static let salad = ImageAsset(name: "salad")
    internal static let spinach = ImageAsset(name: "spinach")
    internal static let springOnion = ImageAsset(name: "spring_onion")
    internal static let strawberry = ImageAsset(name: "strawberry")
    internal static let tamarind = ImageAsset(name: "tamarind")
    internal static let tomato = ImageAsset(name: "tomato")
    internal static let turnip = ImageAsset(name: "turnip")
    internal static let watermelon = ImageAsset(name: "watermelon")
  }
  internal static let lightGreen = ImageAsset(name: "light-green")
  internal static let lightOrange = ImageAsset(name: "light-orange")
  internal static let lightRed = ImageAsset(name: "light-red")
  internal static let lopinSeparator = ImageAsset(name: "lopin-separator")
  internal static let monPotager = ImageAsset(name: "mon potager")
  internal static let splash = ImageAsset(name: "splash")
  internal static let tabbarPotagerSelected = ImageAsset(name: "tabbar-potager-selected")
  internal static let tabbarPotager = ImageAsset(name: "tabbar-potager")
  internal static let tabbarRecetteSelected = ImageAsset(name: "tabbar-recette-selected")
  internal static let tabbarRecette = ImageAsset(name: "tabbar-recette")
  internal static let tomate = ImageAsset(name: "tomate")
  internal static let tomateTuto = ImageAsset(name: "tomateTuto")

  // swiftlint:disable trailing_comma
  internal static let allColors: [ColorAsset] = [
    Color.accentAction,
    Color.bleuFond,
    Color.bleuTitre,
    Color.clearWhite10,
    Color.clearWhite30,
  ]
  internal static let allImages: [ImageAsset] = [
    _001Sun,
    _033Drops,
    guillaumeTPola,
    lineSplashscreen,
    addSeed1,
    addSeed,
    arrow,
    bgPattern,
    checkboxFilled,
    home,
    iconStarEmpty,
    iconStarFull,
    iconUserProfile,
    iconsSplashscreen,
    imageMenuCultiver,
    imageMenuPlanter,
    imageMenuPotager,
    Legume.acorn,
    Legume.ail,
    Legume.apple,
    Legume.apricot,
    Legume.artichoke,
    Legume.asparagus,
    Legume.avocado,
    Legume.bamboo,
    Legume.bananas,
    Legume.bellPepper,
    Legume.betterave,
    Legume.blackberry,
    Legume.blueberry,
    Legume.breastMilkFruit,
    Legume.broccoli,
    Legume.brussels,
    Legume.cabbage,
    Legume.carrot,
    Legume.cauliflower,
    Legume.celery,
    Legume.cherry,
    Legume.chili,
    Legume.coconut,
    Legume.corn,
    Legume.courge,
    Legume.courgette,
    Legume.cucumber,
    Legume.currant,
    Legume.durian,
    Legume.eggplant,
    Legume.figue,
    Legume.ginger,
    Legume.grape,
    Legume.grapefruit,
    Legume.kiwi,
    Legume.lemon,
    Legume.lettuce,
    Legume.lightGreen,
    Legume.lightOrange,
    Legume.lightRed,
    Legume.mandarin,
    Legume.mango,
    Legume.mangosteen,
    Legume.melon,
    Legume.mirabelle,
    Legume.mushroom,
    Legume.olive,
    Legume.onion,
    Legume.orange,
    Legume.papaya,
    Legume.parsley,
    Legume.peach,
    Legume.peanut,
    Legume.pear,
    Legume.peas,
    Legume.pineapple,
    Legume.pitaya,
    Legume.poireau,
    Legume.pomegranate,
    Legume.potato,
    Legume.prune,
    Legume.pumpkin,
    Legume.quince,
    Legume.radish,
    Legume.rambutan,
    Legume.raspberry,
    Legume.rhubarb,
    Legume.roseApple,
    Legume.salad,
    Legume.spinach,
    Legume.springOnion,
    Legume.strawberry,
    Legume.tamarind,
    Legume.tomato,
    Legume.turnip,
    Legume.watermelon,
    lightGreen,
    lightOrange,
    lightRed,
    lopinSeparator,
    monPotager,
    splash,
    tabbarPotagerSelected,
    tabbarPotager,
    tabbarRecetteSelected,
    tabbarRecette,
    tomate,
    tomateTuto,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  internal static let allValues: [MOBXcassetsType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

internal extension Image {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
