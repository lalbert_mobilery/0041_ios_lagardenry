import UIKit

extension Formatter
{
    static let mob_yyyyMMdd: DateFormatter = {
        let tFormatter = DateFormatter()
        tFormatter.locale = Locale.current
        tFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        tFormatter.dateFormat = "yyyy-MM-dd"
        return tFormatter
    }()
    
    static let mob_ddMMyyyy: DateFormatter = {
        let tFormatter = DateFormatter()
        tFormatter.locale = Locale.current
        tFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        tFormatter.dateFormat = "dd/MM/yyyy"
        return tFormatter
    }()
}

extension Date
{
    var mob_ddMMyyyy: String
    {
        return Formatter.mob_ddMMyyyy.string(from: self)
    }
}

extension String
{
    var mob_yyyyMMdd: Date?
    {
        return Formatter.mob_yyyyMMdd.date(from: self)
    }
}
