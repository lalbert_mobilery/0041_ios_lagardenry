//
//  MOBDataProvider.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBDataProvider: NSObject {
    
    let TAG : String = "MOBDataProvider"
    
    let FILE_NAME_DATA : String = "data"
    let FILE_NAME_VEGETABLES : String = "legumes"
    let FILE_NAME_FRUITS : String = "fruits"
    let FILE_EXTENSION : String = "json"
    
    static let shared: MOBDataProvider = MOBDataProvider()
    var mStructureDataDeserialize : MOBDataStructuresWrapper?
    var mStructureLegumesDeserialize : MOBVegetablesStructureWrapper?
    var mStructureFruitsDeserialize : MOBFruitsStructureWrapper?
    
    func extractLocalJsonFiles() {
        self.parseJsonData()
        self.parseJsonVegetables()
        self.parseJsonFruits()
    }
    
    func parseJsonData(){
        
        if let tData: Data = self.getFileContent(fileName: FILE_NAME_DATA, withExtension: FILE_EXTENSION)
        {
            let tJSONDecoder: JSONDecoder = JSONDecoder()
            
            do{
                let tDataStructure : MOBDataStructuresWrapper = try tJSONDecoder.decode(MOBDataStructuresWrapper.self, from: tData)
                
                if( tDataStructure != nil)
                {
                    print(TAG + " [SUCCESS] parseJsonData : Data conversion réussie")
                    self.mStructureDataDeserialize = tDataStructure
                }else{
                    print(TAG + " [FAIL] parseJsonData : Data echec conversion")
                }
                
            }catch (let tError){
                print(TAG + " [ERROR] parseJsonData : " + tError.localizedDescription )
            }
        }
    }
    
    func parseJsonVegetables(){
        
        if let tData: Data = self.getFileContent(fileName: FILE_NAME_VEGETABLES, withExtension: FILE_EXTENSION)
        {
            let tJSONDecoder: JSONDecoder = JSONDecoder()
            
            do{
                let tVegetablesStructure : MOBVegetablesStructureWrapper = try tJSONDecoder.decode(MOBVegetablesStructureWrapper.self, from: tData)
                
                if( tVegetablesStructure != nil)
                {
                    print(TAG + " [SUCCESS] parseJsonVegetables : Legume conversion réussie")
                    self.mStructureLegumesDeserialize = tVegetablesStructure
                }else{
                    print(TAG + " [FAIL] parseJsonVegetables : Legume echec conversion")
                }
                
            }catch (let tError){
                print(TAG + " [ERROR] parseJsonVegetables : " + tError.localizedDescription )
            }
        }
    }
    
    func parseJsonFruits(){
        
        if let tData: Data = self.getFileContent(fileName: FILE_NAME_FRUITS, withExtension: FILE_EXTENSION)
        {
            let tJSONDecoder: JSONDecoder = JSONDecoder()
            
            do{
                let tFruitsStructure : MOBFruitsStructureWrapper = try tJSONDecoder.decode(MOBFruitsStructureWrapper.self, from: tData)
                
                if( tFruitsStructure != nil)
                {
                    print(TAG + " [SUCCESS] parseJsonFruits : Fruits conversion réussie")
                    self.mStructureFruitsDeserialize = tFruitsStructure
                }else{
                    print(TAG + " [FAIL] parseJsonFruits : Fruits echec conversion")
                }
                
            }catch (let tError){
                print(TAG + " [ERROR] parseJsonFruits : " + tError.localizedDescription )
            }
        }
    }
    
    
    
    
    fileprivate func getFileContent(fileName sFileName: String, withExtension sFileExtension: String) -> Data?
    {
        let tUrl = Bundle.main.url(forResource: sFileName, withExtension: sFileExtension)
        do
        {
            let tData = try Data(contentsOf: tUrl!)
            return tData
        }
        catch (let tError)
        {
            print("Media Parser error: \(tError.localizedDescription)")
            return nil
        }
    }
}
