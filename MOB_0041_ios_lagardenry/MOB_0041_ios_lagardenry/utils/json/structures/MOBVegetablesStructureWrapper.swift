//
//  MOBDataStructures.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class MOBVegetablesStructureWrapper : Decodable
{
    var mListVegetables : [MOBVegetable]
    
    required init(from decoder: Decoder) throws {
        
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mListVegetables = try tValues.decode([MOBVegetable].self, forKey: .mListVegetables)
        
    }
    
    enum CodingKeys: String, CodingKey
    {
        case mListVegetables = "legumes"
    }
}
