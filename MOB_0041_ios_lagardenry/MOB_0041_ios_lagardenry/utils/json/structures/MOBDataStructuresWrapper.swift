//
//  MOBDataStructures.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class MOBDataStructuresWrapper : Decodable
{
    var mUser : MOBUser?
    var mGarden : MOBGarden?
    
    required init(from decoder: Decoder) throws {
        
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mUser = try tValues.decode(MOBUser.self, forKey: .mUser)
        self.mGarden = try tValues.decode(MOBGarden.self, forKey: .mGarden)
        
    }
    
    enum CodingKeys: String, CodingKey
    {
        case mUser = "user"
        case mGarden = "garden"
    }
}
