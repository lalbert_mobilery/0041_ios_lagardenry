//
//  VegetablesServiceTest.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 09/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class VegetablesServiceTest: NSObject {
    
    func testGetAllVegetableAndFruits()
    {
        var tServices :  MOBVegetableFruitService = MOBVegetableFruitService.mShared
        
        var tListAll : [MOBVegetable] = tServices.getAllVegetablesAndFruits();
    }
    
    func testGetVegetables()
    {
        var tServices :  MOBVegetableFruitService = MOBVegetableFruitService.mShared
        
        var tListVegetables : [MOBVegetable] = tServices.getAllVegetableOnly();
    }
    
    func testGetFruits()
    {
        var tServices :  MOBVegetableFruitService = MOBVegetableFruitService.mShared
        
        var tListFruits : [MOBVegetable] = tServices.getAllFruitsOnly();
    }
}
