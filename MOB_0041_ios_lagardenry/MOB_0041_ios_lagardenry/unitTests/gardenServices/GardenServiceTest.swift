//
//  GardenServiceTest.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 09/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class GardenServiceTest{
    
    static let mShared : GardenServiceTest = GardenServiceTest()
    
    func testAllService(){
        var tPlot = MOBPlot();
        var tVegetable = MOBVegetable()
        tVegetable.mName = "pomme"
        tPlot.mIdPlot = "ID_PLOT_004"
        tPlot.mVegetable = tVegetable
        tPlot.mPlantingDate = Date()
        
        var tListSlot : [MOBSLot] = MOBGardenService.mShared.getAllSlotAvailable()
        
        MOBGardenService.mShared.addPlot(SlotName: "slot 1", NewPLot: tPlot)
        
        tListSlot = MOBGardenService.mShared.getAllSlotAvailable()
        
        MOBGardenService.mShared.removePlot(SlotName: "slot 1", OldPlot: tPlot)
        
        tListSlot = MOBGardenService.mShared.getAllSlotAvailable()
    }

}
