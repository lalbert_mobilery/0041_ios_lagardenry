//
//  RecetteViewController.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBRecetteViewController: MOBViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mNavigationBarVisible = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
