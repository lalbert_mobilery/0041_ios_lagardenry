//
//  PotagerViewController.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBPotagerViewController: MOBViewController {
    
    @IBOutlet weak var mButton1: UIButton!
    
    @IBOutlet weak var mButton2: UIButton!
    
    @IBOutlet weak var mButton3: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mNavigationBarVisible = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addShadow(mButton1)
        self.addShadow(mButton2)
        self.addShadow(mButton3)
    }
    
    func addShadow(_ button: UIButton)
    {
        button.titleLabel!.layer.shadowColor = UIColor.black.cgColor
        button.titleLabel!.layer.shadowOffset = CGSize(width: 0, height: 3)
        button.titleLabel!.layer.shadowOpacity = 1
        button.titleLabel!.layer.shadowRadius = 1
        button.titleLabel!.layer.masksToBounds = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
