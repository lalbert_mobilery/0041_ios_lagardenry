//
//  MOBGestionSpaceTableViewCell.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBChoixTableViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mImage: UIImageView!
    
    static let kCellIdentifier: String = "MOBChoixTableViewCell"
    
    weak var mController : UIViewController?
    
    func display (sController : UIViewController)
    {
        mController = sController
    }
    
    @IBAction func click(_ sender: Any)
    {
        mController?.navigationController?.popViewController(animated: true)
    }
}
