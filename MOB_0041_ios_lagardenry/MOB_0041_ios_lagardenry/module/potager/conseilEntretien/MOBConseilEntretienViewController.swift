//
//  MOBConseilEntretienViewController.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBConseilEntretienViewController: MOBViewController {
    
    @IBOutlet weak var mCollection: UICollectionView!
    
    @IBOutlet weak var mButton1: UIButton!
    @IBOutlet weak var mButton2: UIButton!
    
    var mFruitSlots : [MOBVegetable] = MOBVegetableFruitService.init().getAllFruitsOnly()
    var mVegetableSlots : [MOBVegetable] = MOBVegetableFruitService.init().getAllVegetableOnly()
    
    var mCurrent : [MOBVegetable]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mCurrent = mFruitSlots
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        let tAttributDefault = [NSAttributedString.Key.font: UIFont(font: MOBFonts.Bodoni72.bold, size: 15.0)!,
                                NSAttributedString.Key.foregroundColor: UIColor(named: "BleuTitre")]
        let tAttributSelected = [NSAttributedString.Key.font: UIFont(font: MOBFonts.Bodoni72.bold, size: 15.0)!,
                                 NSAttributedString.Key.foregroundColor: UIColor(named: "AccentAction")]
        
        let tTitle1 = NSAttributedString(string: "DANS LE POTAGER", attributes: tAttributDefault as [NSAttributedString.Key : Any])
        let tTitle1Selected = NSAttributedString(string: "DANS LE POTAGER", attributes: tAttributSelected as [NSAttributedString.Key : Any])
        mButton1.setAttributedTitle(tTitle1, for: .normal)
        mButton1.setAttributedTitle(tTitle1Selected, for: .selected)
        mButton1.borderWidth = 1
        mButton1.borderColor = UIColor(named: "AccentAction")
        
        let tTitle2  = NSAttributedString(string: "TOUS", attributes: tAttributDefault as [NSAttributedString.Key : Any])
        let tTitle2Selected  = NSAttributedString(string: "TOUS", attributes: tAttributSelected as [NSAttributedString.Key : Any])
        mButton2.setAttributedTitle(tTitle2, for: .normal)
        mButton2.setAttributedTitle(tTitle2Selected, for: .selected)
        mButton2.borderWidth = 1
        mButton2.borderColor = UIColor.clear
        
        mCollection.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func clickButton(_ sender: UIButton) {
        if sender == mButton1
        {
            mButton1.isSelected = true
            mButton1.borderColor = UIColor(named: "AccentAction")
            mButton2.isSelected = false
            mButton2.borderColor = UIColor.clear
            mCurrent = mFruitSlots
        }
        else if sender == mButton2
        {
            mButton1.isSelected = false
            mButton1.borderColor = UIColor.clear
            mButton2.isSelected = true
            mButton2.borderColor = UIColor(named: "AccentAction")
            mCurrent = mVegetableSlots
        }
        mCollection.reloadData()
    }
}

extension MOBConseilEntretienViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mCurrent!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let tCell: MOBConseilEntretienTableViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MOBConseilEntretienTableViewCell.kCellIdentifier, for: indexPath) as! MOBConseilEntretienTableViewCell
        
        tCell.mImage.image = UIImage(named: mCurrent![indexPath.row].mImgUrl)
        
        return tCell
    }
}
