//
//  MOBGestionSpaceViewController.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBGestionSpaceViewController: MOBViewController {
    
    @IBOutlet weak var mCollection: UICollectionView!
    
    var mListSlots : [MOBSLot] = MOBGardenService.init().getAllSlotAvailable()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mCollection.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension MOBGestionSpaceViewController : UICollectionViewDelegate, UICollectionViewDataSource
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mListSlots.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let tCell: MOBGestionSpaceTableViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: MOBGestionSpaceTableViewCell.kCellIdentifier, for: indexPath) as! MOBGestionSpaceTableViewCell
        
        let tSlot : MOBSLot = mListSlots[indexPath.row]
        
        tCell.displayInfo(sSLot: tSlot, sController: self)
        
        return tCell
    }
}
