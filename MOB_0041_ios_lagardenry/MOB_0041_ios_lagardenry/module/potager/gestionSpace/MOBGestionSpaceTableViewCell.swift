//
//  MOBGestionSpaceTableViewCell.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Loïc Albert on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBGestionSpaceTableViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mLabel: UILabel!
    
    @IBOutlet weak var mImage1Water: UIImageView!
    @IBOutlet weak var mImage1: UIImageView!
    @IBOutlet weak var mLevelOK1: UIImageView!
    @IBOutlet weak var mLevelAverage1: UIImageView!
    @IBOutlet weak var mLevelKO1: UIImageView!
    @IBOutlet weak var mAdd1: UIButton!
    @IBOutlet weak var mRemove1: UIButton!
    
    @IBOutlet weak var mImage2: UIImageView!
    @IBOutlet weak var mLevelOK2: UIImageView!
    @IBOutlet weak var mLevelAverage2: UIImageView!
    @IBOutlet weak var mLevelKO2: UIImageView!
    @IBOutlet weak var mAdd2: UIButton!
    @IBOutlet weak var mRemove2: UIButton!
    
    @IBOutlet weak var mImage3: UIImageView!
    @IBOutlet weak var mLevelOK3: UIImageView!
    @IBOutlet weak var mLevelAverage3: UIImageView!
    @IBOutlet weak var mLevelKO3: UIImageView!
    @IBOutlet weak var mAdd3: UIButton!
    @IBOutlet weak var mRemove3: UIButton!
    
    @IBOutlet weak var mImage4: UIImageView!
    @IBOutlet weak var mLevelOK4: UIImageView!
    @IBOutlet weak var mLevelAverage4: UIImageView!
    @IBOutlet weak var mLevelKO4: UIImageView!
    @IBOutlet weak var mAdd4: UIButton!
    @IBOutlet weak var mRemove4: UIButton!
    
    weak var mController: UIViewController?
    
    static let kCellIdentifier: String = "MOBGestionSpaceTableViewCell"
    
    func displayInfo(sSLot : MOBSLot, sController: UIViewController)
    {
        mController = sController
        var sPlot : MOBPlot = sSLot.mListPlots[0]
        mLabel.text = sSLot.mName
        switch sPlot.mMaturity {
        case .EMPTY:
            mLevelOK1.isHidden = true
            mLevelAverage1.isHidden = true
            mLevelKO1.isHidden = true
            mAdd1.isHidden = false
            mRemove1.isHidden = true
            mImage1.isHidden = true
        case .BEGIN:
            mLevelOK1.isHidden = true
            mLevelAverage1.isHidden = true
            mLevelKO1.isHidden = false
            mAdd1.isHidden = true
            mRemove1.isHidden = false
            mImage1.isHidden = false
        case .IN_PROGRESS:
            mLevelOK1.isHidden = true
            mLevelAverage1.isHidden = false
            mLevelKO1.isHidden = false
            mAdd1.isHidden = true
            mRemove1.isHidden = false
            mImage1.isHidden = false
        case .FINISH:
            mLevelOK1.isHidden = false
            mLevelAverage1.isHidden = false
            mLevelKO1.isHidden = false
            mAdd1.isHidden = true
            mRemove1.isHidden = false
            mImage1.isHidden = false
        }
        if let tImage = sPlot.mVegetable?.mImgUrl
        {
            mImage1.image = UIImage(named: tImage)
            mImage1.isHidden = false
            mAdd1.isHidden = true
            mRemove1.isHidden = false
        }
        else
        {
            mImage1.isHidden = true
            mAdd1.isHidden = false
            mRemove1.isHidden = true
        }
        sPlot = sSLot.mListPlots[1]
        switch sPlot.mMaturity {
        case .EMPTY:
            mLevelOK2.isHidden = true
            mLevelAverage2.isHidden = true
            mLevelKO2.isHidden = true
            mAdd2.isHidden = false
            mRemove2.isHidden = true
            mImage2.isHidden = true
        case .BEGIN:
            mLevelOK2.isHidden = true
            mLevelAverage2.isHidden = true
            mLevelKO2.isHidden = false
            mAdd2.isHidden = true
            mRemove2.isHidden = false
            mImage2.isHidden = false
        case .IN_PROGRESS:
            mLevelOK2.isHidden = true
            mLevelAverage2.isHidden = false
            mLevelKO2.isHidden = false
            mAdd2.isHidden = true
            mRemove2.isHidden = false
            mImage2.isHidden = false
        case .FINISH:
            mLevelOK2.isHidden = false
            mLevelAverage2.isHidden = false
            mLevelKO2.isHidden = false
            mAdd2.isHidden = true
            mRemove2.isHidden = false
            mImage2.isHidden = false
        }
        if let tImage = sPlot.mVegetable?.mImgUrl
        {
            mImage2.image = UIImage(named: tImage)
            mImage2.isHidden = false
            mAdd2.isHidden = true
            mRemove2.isHidden = false
        }
        else
        {
            mImage2.isHidden = true
            mAdd2.isHidden = false
            mRemove2.isHidden = true
        }
        sPlot = sSLot.mListPlots[2]
        switch sPlot.mMaturity {
        case .EMPTY:
            mLevelOK3.isHidden = true
            mLevelAverage3.isHidden = true
            mLevelKO3.isHidden = true
            mAdd3.isHidden = false
            mRemove3.isHidden = true
            mImage3.isHidden = false
        case .BEGIN:
            mLevelOK3.isHidden = true
            mLevelAverage3.isHidden = true
            mLevelKO3.isHidden = false
            mAdd3.isHidden = true
            mRemove3.isHidden = false
            mImage3.isHidden = false
        case .IN_PROGRESS:
            mLevelOK3.isHidden = true
            mLevelAverage3.isHidden = false
            mLevelKO3.isHidden = false
            mAdd3.isHidden = true
            mRemove3.isHidden = false
            mImage3.isHidden = false
        case .FINISH:
            mLevelOK3.isHidden = false
            mLevelAverage3.isHidden = false
            mLevelKO3.isHidden = false
            mAdd3.isHidden = true
            mRemove3.isHidden = false
            mImage3.isHidden = false
        }
        if let tImage = sPlot.mVegetable?.mImgUrl
        {
            mImage3.image = UIImage(named: tImage)
            mImage3.isHidden = false
            mAdd3.isHidden = true
            mRemove3.isHidden = false
        }
        else
        {
            mImage3.isHidden = true
            mAdd3.isHidden = false
            mRemove3.isHidden = true
        }
        sPlot = sSLot.mListPlots[3]
        switch sPlot.mMaturity {
        case .EMPTY:
            mLevelOK4.isHidden = true
            mLevelAverage4.isHidden = true
            mLevelKO4.isHidden = true
            mAdd4.isHidden = false
            mRemove4.isHidden = true
            mImage4.isHidden = true
        case .BEGIN:
            mLevelOK4.isHidden = true
            mLevelAverage4.isHidden = true
            mLevelKO4.isHidden = false
            mAdd4.isHidden = true
            mRemove4.isHidden = false
            mImage4.isHidden = false
        case .IN_PROGRESS:
            mLevelOK4.isHidden = true
            mLevelAverage4.isHidden = false
            mLevelKO4.isHidden = false
            mAdd4.isHidden = true
            mRemove4.isHidden = false
            mImage4.isHidden = false
        case .FINISH:
            mLevelOK4.isHidden = false
            mLevelAverage4.isHidden = false
            mLevelKO4.isHidden = false
            mAdd4.isHidden = true
            mRemove4.isHidden = false
            mImage4.isHidden = false
        }
        if let tImage = sPlot.mVegetable?.mImgUrl
        {
            mImage4.image = UIImage(named: tImage)
            mImage4.isHidden = false
            mAdd4.isHidden = true
            mRemove4.isHidden = false
        }
        else
        {
            mImage4.isHidden = true
            mAdd4.isHidden = false
            mRemove4.isHidden = true
        }
        
    }
    
    @IBAction func clickAdd(_ sender: UIButton)
    {
        mLevelOK4.isHidden = true
        mLevelAverage4.isHidden = true
        mLevelKO4.isHidden = false
        mAdd4.isHidden = true
        mRemove4.isHidden = false
        mImage4.isHidden = false
        mImage4.image = UIImage(named: "lemon")
    }
    
    @IBAction func clickVegetable(_ sender: UIButton)
    {
        let tAlert : UIAlertController = UIAlertController(title:"ACTION",
                                                           message: "",
                                                           preferredStyle: .actionSheet)
        tAlert.addAction(UIAlertAction(title: "Annuler",
                                       style: .cancel,
                                       handler: { (cAction) in
                                        tAlert.dismiss(animated: true)
                                        
        }))
        tAlert.addAction(UIAlertAction(title: "J'ai arrosé",
                                       style: .default,
                                       handler: { (cAction) in
                                        tAlert.dismiss(animated: true)
                                        self.mImage1Water.isHidden = true
        }))
        tAlert.addAction(UIAlertAction(title: "J'ai récolté",
                                       style: .destructive,
                                       handler: { (cAction) in
                                        tAlert.dismiss(animated: true)
                                        self.mImage1Water.isHidden = false
                                        self.mImage1.isHidden = true
                                        self.mAdd1.isHidden = false
                                        self.mLevelKO1.superview?.isHidden = true
                                        self.mImage1Water.isHidden = true
        }))
        mController!.present(tAlert, animated: true)
    }
}
