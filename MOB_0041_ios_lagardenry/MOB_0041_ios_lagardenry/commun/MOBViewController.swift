import Foundation
import UIKit
import SnapKit

class MOBViewController: UIViewController
{
    var mNavigationBarVisible : Bool = true
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setupNavBar(animated)
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var previewActionItems: [UIPreviewActionItem] {
        
        let likeAction = UIPreviewAction(title: "S'OCCUPER DU POTAGER", style: .default) { (action, viewController) -> Void in

        }
        
        return [likeAction]
        
    }
}

// Custom Bar
extension MOBViewController
{
    func setupNavBar(_ animated: Bool)
    {
        if let tNavController = self.navigationController as? MOBNavigationController
        {
            tNavController.setupNavBar(animated)
            tNavController.setNavigationBarHidden(!mNavigationBarVisible, animated: animated)
            
            let tTitleLabel : UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            tTitleLabel.font = UIFont(font: MOBFonts.Bodoni72.book, size: 28.0)
            tTitleLabel.textColor = UIColor(named: "BleuTitre")
            tTitleLabel.text = self.title
            tTitleLabel.textAlignment = .center
            tTitleLabel.numberOfLines = 1
            
            let tTitleView = UIView(frame: CGRect())
            tTitleView.addSubview(tTitleLabel)
            
            tTitleLabel.snp.makeConstraints { (tMake) in
                tMake.top.equalToSuperview()
                tMake.right.equalToSuperview()
                tMake.left.equalToSuperview()
                tMake.bottom.equalToSuperview()
            }
            
            navigationItem.titleView = tTitleView
        }
    }
}
