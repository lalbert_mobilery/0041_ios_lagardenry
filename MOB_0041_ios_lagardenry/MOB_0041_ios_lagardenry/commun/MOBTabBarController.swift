import Foundation
import UIKit
import SnapKit

class MOBTabBarController: UITabBarController
{
    var mNavigationBarVisible : Bool = true
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setupTabBar(animated)
    }
}

// Custom Bar
extension MOBTabBarController
{
    func setupTabBar(_ animated: Bool)
    {
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : UIFont(font: MOBFonts.Bodoni72.bold, size: 8.0)], for: .selected)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font : UIFont(font: MOBFonts.Bodoni72.book, size: 8.0)], for: .normal)
        
//        UITabBar.appearance().tintColor = Color(named: MOBColors.ColorsSpecific.titleTabBarItemColorSelected)
//        UITabBar.appearance().unselectedItemTintColor = Color(named: MOBColors.ColorsSpecific.titleTabBarItemColorUnSelected)

        self.tabBar.isTranslucent = true
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        UITabBar.appearance().tintColor = UIColor(named: "ClearWhite10")
        UITabBar.appearance().backgroundColor = UIColor(named: "ClearWhite10")
    }
}

extension MOBTabBarController
{
    
}
