import Foundation
import UIKit

class MOBNavigationController: UINavigationController
{
    var mNavigationBarVisible = false
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
        setupNavBar(animated)
    }
}

// Custom Bar
extension MOBNavigationController
{
    func setupNavBar(_ animated: Bool)
    {
        navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(font: MOBFonts.Bodoni72.bold, size: 28.0)!,
                                             NSAttributedString.Key.foregroundColor: UIColor(named: "BleuTitre")!]
        UINavigationBar.appearance().tintColor = UIColor(named: "BleuFond")
        UINavigationBar.appearance().backgroundColor = UIColor(named: "BleuFond")
        navigationBar.barTintColor = UIColor(named: "BleuFond")
        
        UINavigationBar.appearance().shadowImage = UIImage()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(font: MOBFonts.Bodoni72.bold, size: 28.0)!,], for: .normal)
    }
}
