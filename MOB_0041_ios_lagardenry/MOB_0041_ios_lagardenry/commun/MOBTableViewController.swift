import Foundation
import UIKit
import SnapKit

class MOBTableViewController: UITableViewController
{
    var mNavigationBarVisible : Bool = true
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        setupNavBar(animated)
    }
}

// Custom Bar
extension MOBTableViewController
{
    func setupNavBar(_ animated: Bool)
    {
        if let tNavController = self.navigationController as? MOBNavigationController
        {
            tNavController.setupNavBar(animated)
            tNavController.setNavigationBarHidden(!mNavigationBarVisible, animated: animated)
        }
    }
}
