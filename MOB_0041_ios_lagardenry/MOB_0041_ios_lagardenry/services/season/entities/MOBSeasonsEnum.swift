//
//  MOBSeasons.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

enum MOBSeasonEnum{
    
    case AUTUMN
    case WINTER
    case SPRING
    case SUMMER
}
