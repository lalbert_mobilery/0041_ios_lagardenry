//
//  MOBMounth.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

enum MOBMonthEnum : Int {
    
    case JANUARY = 0
    case FEBRUARY = 1
    case MARCH = 2
    case APRIL = 3
    case MAY = 4
    case JUNE = 5
    case JULY = 6
    case AUGUST = 7
    case SEPTEMBER = 8
    case OCTOBER = 9
    case NOVEMBER = 10
    case DECEMBER = 11
}
