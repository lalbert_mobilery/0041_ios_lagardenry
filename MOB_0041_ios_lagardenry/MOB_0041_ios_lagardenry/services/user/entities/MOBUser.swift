//
//  MOBUser.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBUser : Decodable{
    
    var mFirstName : String = ""
    var mLastName : String = ""
    var mScore : Int = 0
    
    required init(from decoder: Decoder) throws{
        
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mFirstName = try tValues.decode(String.self, forKey: .mFirstName)
        self.mLastName = try tValues.decode(String.self, forKey: .mLastName)
        self.mScore = try tValues.decode(Int.self, forKey: .mScore)
    }
    
    enum CodingKeys: String, CodingKey
    {
        case mFirstName = "firstname"
        case mLastName = "lastname"
        case mScore = "score"
    }
}
