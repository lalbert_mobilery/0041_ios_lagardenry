//
//  MOBVegetable.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBVegetable : Decodable , Equatable{
    
    var mName : String = ""
    var mImgUrl : String = ""
    var mMonth : [Int] = []
    
    init() {
        
    }
    
    required init(from decoder: Decoder) throws{
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mName = try tValues.decode(String.self, forKey: .mName)
        self.mImgUrl = try tValues.decode(String.self, forKey: .mImgUrl)
        self.mMonth = try tValues.decode([Int].self, forKey: .mMonth)
    }
    
    enum CodingKeys : String, CodingKey {
        case mName = "name"
        case mImgUrl = "img"
        case mMonth = "month"
    }
    
    static func == (lhs: MOBVegetable, rhs: MOBVegetable) -> Bool {
        var result : Bool = false
        if(lhs != nil && rhs != nil && lhs.mName.elementsEqual(rhs.mName)){
            result = true
        }
        return result
    }
}
