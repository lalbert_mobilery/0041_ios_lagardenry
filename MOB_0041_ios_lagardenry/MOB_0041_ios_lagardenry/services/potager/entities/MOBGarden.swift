//
//  MOBGarden.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBGarden : Decodable{
    
    var mSize : Int = 0
    var mColumn : Int = 0
    var mLine : Int = 0
    var mListSlots : [MOBSLot] = []
    
    required init(from decoder: Decoder) throws {
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
    
        self.mSize = try tValues.decode(Int.self, forKey: .mSize)
        self.mColumn = try tValues.decode(Int.self, forKey: .mColumn)
        self.mLine = try tValues.decode(Int.self, forKey: .mLine)
        self.mListSlots = try tValues.decode([MOBSLot].self, forKey: .mListSlots)
    }
    
    enum CodingKeys: String, CodingKey{
        case mSize = "size"
        case mColumn = "column"
        case mLine = "line"
        case mListSlots = "slots"
    }
    
    func cleanSlot(){
        for tSlot in self.mListSlots{
            tSlot.cleanEmptyPlot()
        }
    }
}
