//
//  MOBSlot.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
class MOBSLot : Decodable {
    
    var mName : String = ""
    var mNumberPlots : Int = 0
    var mListPlots : [MOBPlot] = []
    
    init()
    {
        
    }
    
    required init(from decoder: Decoder) throws{
        //keynote Guillaume
        //listPlots.append(MOBPlot())
        
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mName = try tValues.decode(String.self, forKey: .mName)
        self.mNumberPlots = try tValues.decode(Int.self, forKey: .mNumberPlots)
        self.mListPlots = try tValues.decode([MOBPlot].self, forKey: .mListPlots)
    
    }
    
    enum CodingKeys: String, CodingKey{
        case mName = "name"
        case mNumberPlots = "number_plots"
        case mListPlots = "list_plot"
    }
    
    var hashValue: Int {
        return mName.hashValue * -5646536 + ("MOBSLot_SLOT"+mName).hashValue
    }
    
    
    func containsPlot(Plot sPlot : MOBPlot) -> Bool {
        var result : Bool = false
        if(self.mListPlots != nil){
            for tPlot in self.mListPlots
            {
                if(tPlot.mIdPlot.elementsEqual(sPlot.mIdPlot))
                {
                    result = true
                    break
                }
            }
        }
        return result
    }
    
    func getIndexOfPlot(Plot sPlot : MOBPlot) -> Int{
        var rIndex : Int = -1
        
        if(self.containsPlot(Plot: sPlot)){
            var i : Int = 0
            while i < self.mListPlots.count
            {
                let tPlot : MOBPlot = self.mListPlots[i]
                if(tPlot.mIdPlot.elementsEqual(sPlot.mIdPlot))
                {
                    rIndex = i
                    break
                }
                i += 1
            }
        }
        return rIndex
    }
    
    func cleanEmptyPlot(){
        if(self.mListPlots != nil){
            var i : Int = 0
            var tIndexEmptyList :[Int] = []
            
            while i < self.mListPlots.count {
               var tPlot = mListPlots[i]
                if(tPlot.isEmpty()){
                    tIndexEmptyList.append(i)
                }
                i += 1
            }
            tIndexEmptyList.reverse()
            for tIndexToRemove in tIndexEmptyList {
                self.mListPlots.remove(at: tIndexToRemove)
            }
        }
    }
}

