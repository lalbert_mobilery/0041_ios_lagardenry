//
//  MOBPlot.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBPlot : Decodable, Equatable{
    
    var mIdPlot : String = ""
    var mVegetable : MOBVegetable?
    var mPlantingDate : Date?
    var mLastSpray : Date?
    var mMaturity : MOBMaturityEnum = MOBMaturityEnum.BEGIN
    
    init(){
        
    }
    
    required init(from decoder: Decoder) throws{
        let tValues = try decoder.container(keyedBy: CodingKeys.self)
        
        self.mIdPlot = try tValues.decode(String.self, forKey: .mIdPlot)
        self.mVegetable = try tValues.decode(MOBVegetable.self, forKey: .mVegetable)
        let tPlantingDateStr : String = try tValues.decode(String.self, forKey: .mPlantingDate)
        let tLastSprayDateStr : String = try tValues.decode(String.self, forKey: .mLastSpray)
        let tMaturity : Int = try tValues.decode(Int.self, forKey: .mMaturity)
        
        let tConverter = MOBMaturityConverter()
        
        self.mMaturity = tConverter.convertIntToEnum(Value: tMaturity)
        self.mPlantingDate = DateFormatter.mob_yyyyMMdd.date(from: tPlantingDateStr)
        self.mLastSpray = DateFormatter.mob_yyyyMMdd.date(from: tLastSprayDateStr)
    }
    
    enum CodingKeys: String, CodingKey {
        case mIdPlot = "id_plot"
        case mVegetable = "vegetable"
        case mPlantingDate = "planting_date"
        case mLastSpray = "last_spary_date"
        case mMaturity = "maturity"
    }
    
    static func == (lhs: MOBPlot, rhs: MOBPlot) -> Bool {
        var result : Bool = false
        
        if(lhs != nil && rhs != nil && lhs.mIdPlot.elementsEqual(rhs.mIdPlot)){
            result = true
        }
        
        return result
    }
    
    func isEmpty() -> Bool {
        var result = true
        
        if(!self.mIdPlot.isEmpty && self.mVegetable != nil && mPlantingDate != nil){
            result = false
        }
        
        return result
    }
}
