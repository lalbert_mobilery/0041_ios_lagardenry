//
//  MOBMaturityEnum.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

enum MOBMaturityEnum
{
    case EMPTY
    case BEGIN
    case IN_PROGRESS
    case FINISH
}
