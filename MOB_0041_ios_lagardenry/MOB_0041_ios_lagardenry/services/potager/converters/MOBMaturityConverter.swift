//
//  MOBMaturityConverter.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation
import UIKit

class MOBMaturityConverter : NSObject{
    
    func convertIntToEnum(Value sValue : Int) -> MOBMaturityEnum{
        var rEnum : MOBMaturityEnum 
        
        switch sValue {
        case 1:
            rEnum = MOBMaturityEnum.BEGIN
            break
            
        case 2:
            rEnum = MOBMaturityEnum.IN_PROGRESS
            break
            
        case 3:
            rEnum = MOBMaturityEnum.FINISH
            break
        default:
            rEnum = MOBMaturityEnum.EMPTY
        }
        return rEnum
    }
}
