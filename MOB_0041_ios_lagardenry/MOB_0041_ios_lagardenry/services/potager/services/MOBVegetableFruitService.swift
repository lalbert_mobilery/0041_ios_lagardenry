//
//  MOBVegetableFruitService.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 09/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class MOBVegetableFruitService: NSObject {
    
    static let mShared : MOBVegetableFruitService = MOBVegetableFruitService()
    
    var mListFruits : [MOBVegetable]?
    var mListVegetables : [MOBVegetable]?
    
    override init() {
        var tProvider : MOBDataProvider = MOBDataProvider.shared
        if( tProvider != nil){
            self.mListVegetables = tProvider.mStructureLegumesDeserialize?.mListVegetables
            self.mListFruits = tProvider.mStructureFruitsDeserialize?.mListFruits
        }
    }
    
    func getAllVegetablesAndFruits() -> [MOBVegetable]{
        var rListAll : [MOBVegetable] = []
        
        if(self.mListVegetables != nil)
        {
            rListAll.append(contentsOf : self.mListVegetables!)
        }
        
        if(self.mListFruits != nil)
        {
            rListAll.append(contentsOf :self.mListFruits!)
        }
        
        return rListAll
    }
    
    
    func getAllVegetableOnly() -> [MOBVegetable]{
        var rListVegetables : [MOBVegetable] = []
        
        if(self.mListVegetables != nil)
        {
            rListVegetables.append(contentsOf : self.mListVegetables!)
        }
        
        return rListVegetables
    }
    
    func getAllFruitsOnly() -> [MOBVegetable]{
        var rListFruits : [MOBVegetable] = []
        
        if(self.mListFruits != nil)
        {
            rListFruits.append(contentsOf : self.mListFruits!)
        }
        
        return rListFruits
    }
}
