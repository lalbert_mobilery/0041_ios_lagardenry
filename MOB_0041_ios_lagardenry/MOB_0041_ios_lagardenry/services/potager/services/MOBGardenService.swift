//
//  MOBGardenInterface.swift
//  MOB_0041_ios_lagardenry
//
//  Created by Tartara Guillaume on 08/11/2018.
//  Copyright © 2018 Loïc Albert. All rights reserved.
//

import Foundation

class MOBGardenService: NSObject {
    
    let NB_GARDEN_CONTRIBUTOR : Int = 12
    
    static let mShared : MOBGardenService = MOBGardenService()
    
    var mCurrentGarden : MOBGarden?
    
    override init() {
        //let tSlotDictionary: [MOBSLot : [MOBSLot]] = [:] //keynote
        let tProvider : MOBDataProvider = MOBDataProvider.shared
        if(tProvider != nil && tProvider.mStructureDataDeserialize != nil){
            self.mCurrentGarden = tProvider.mStructureDataDeserialize?.mGarden
        }
    }
    
    // Permet de retourner le nombre de slot disponible dans le jardin
    // toutes les parcelles sont completées -> une parcelle complete (avec legume)
    //                                      -> une parcelle vide (legume = nil)
    func getAllSlotAvailable() -> [MOBSLot]{
        
        var rSlotsList = [MOBSLot]()

        if(self.mCurrentGarden?.mListSlots != nil){
            
            mCurrentGarden?.cleanSlot()
            
            rSlotsList = mCurrentGarden!.mListSlots
            
            // on complete les slots vides
            for tSlot in rSlotsList {
                var tListPlot : [MOBPlot] = tSlot.mListPlots
                
                if(tListPlot.count < tSlot.mNumberPlots) {
                    let tDiff : Int = tSlot.mNumberPlots - tListPlot.count
                    var i = 0
                    
                    while i < tDiff {
                        tListPlot.append(MOBPlot())
                        i += 1
                    }
                }
                tSlot.mListPlots = tListPlot
            }
        }
        return rSlotsList
    }
    
    // Permet d'ajouter une nouvelle plantation si le slot n'est pas complet
    func addPlot(SlotName sSlotName : String, NewPLot sNewPlot : MOBPlot){
        if(!sSlotName.isEmpty && sNewPlot != nil && self.mCurrentGarden?.mListSlots != nil){
            for tSlot in self.mCurrentGarden!.mListSlots{
                if(sSlotName.elementsEqual(tSlot.mName)){
                    tSlot.cleanEmptyPlot()
                    if(tSlot.mListPlots.count < tSlot.mNumberPlots)
                    {
                        tSlot.mListPlots.append(sNewPlot)
                    }
                    break
                }
            }
        }
    }
    
    // Permet de supprimer une parcelle dans un slot
    func removePlot(SlotName sSlotName : String, OldPlot sOldPlot : MOBPlot){
        if(!sSlotName.isEmpty && sOldPlot != nil && self.mCurrentGarden?.mListSlots != nil){
            for tSlot in self.mCurrentGarden!.mListSlots
            {
                if(tSlot.mName.elementsEqual(sSlotName))
                {
                    var tIndex : Int = tSlot.getIndexOfPlot(Plot: sOldPlot)
                    if( tIndex > -1)
                    {
                        tSlot.mListPlots.remove(at: tIndex)
                    }
                    break
                }
            }
        }
    }
    
    // Permet de retourner le nombre de participant au potager
    func getNumberOfContributorInGarden() -> Int{
        return NB_GARDEN_CONTRIBUTOR
    }
    
}
